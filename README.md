# Multimedia forensics tutorial

In this tutorial we show some Multimedia Forensics applications. In particular we will implement a model called Forensic Similarity in Tensorflow 2 and a simple application of this model on three scenarios:

1. Image forgery detection
2. Camera model identification
3. GAN-generated images detection

The code is based on the official repository of the authors https://gitlab.com/omayer/forensic-graph/.

