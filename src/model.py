import tensorflow as tf

from tensorflow.keras.layers import Conv2D, BatchNormalization, MaxPool2D, Dense, Flatten, Concatenate, Multiply, Reshape
from tensorflow.keras import Model


class MISLNet(Model):
    def __init__(self, input_size=128, nprefilt=6):
        super(MISLNet, self).__init__()
        w_inititalizer = tf.keras.initializers.VarianceScaling(scale=1.0, mode="fan_avg", distribution="uniform")

        # CONV layers
        self.constrained = Conv2D(nprefilt, (5,5), kernel_initializer=w_inititalizer, use_bias=False, name="constrained", padding="valid")

        self.conv1 = Conv2D(96, (7,7), strides=(2,2), kernel_initializer=w_inititalizer, bias_initializer="zeros", name="conv1", padding="same")
        self.bn1 = BatchNormalization(axis=-1, center=True, scale=True, name="bn1")
        self.maxpool1 = MaxPool2D(pool_size=(3, 3), strides=(2,2), padding='valid', name="maxpool1")

        self.conv2 = Conv2D(64, (5,5), kernel_initializer=w_inititalizer, bias_initializer="zeros", name="conv2", padding="same")
        self.bn2 = BatchNormalization(axis=-1, center=True, scale=True, name="bn2")
        self.maxpool2 = MaxPool2D(pool_size=(3, 3), strides=(2,2), padding='valid', name="maxpool2")

        self.conv3 = Conv2D(64, (5,5), kernel_initializer=w_inititalizer, bias_initializer="zeros", name="conv3", padding="same")
        self.bn3 = BatchNormalization(axis=-1, center=True, scale=True, name="bn3")
        self.maxpool3 = MaxPool2D(pool_size=(3, 3), strides=(2,2), padding='valid', name="maxpool3")

        self.conv4 = Conv2D(128, (1,1), kernel_initializer=w_inititalizer, bias_initializer="zeros", name="conv4", padding="same")
        self.bn4 = BatchNormalization(axis=-1, center=True, scale=True, name="bn4")
        self.maxpool4 = MaxPool2D(pool_size=(3, 3), strides=(2,2), padding='valid', name="maxpool4")

        # FC layers
        self.reshape =  Flatten()
        # if input_size == 128:
        #     self.reshape = Reshape((-1, 2 * 2 * 128))
        # elif input_size == 256:
        #     self.reshape = Reshape((-1, 6 * 6 * 128))
        # else:
        #     raise TypeError('Unsupported patch size {}'.format(input_size))

        self.fc1 = Dense(200, kernel_initializer=w_inititalizer, bias_initializer="zeros", name="fc1")
        self.fc2 = Dense(200, kernel_initializer=w_inititalizer, bias_initializer="zeros", name="fc2")

    def call(self, x):
        x = self.constrained(x)

        # CONV layers
        x = self.conv1(x)
        x = self.bn1(x)
        x = tf.nn.tanh(x)
        x = self.maxpool1(x)

        x = self.conv2(x)
        x = self.bn2(x)
        x = tf.nn.tanh(x)
        x = self.maxpool2(x)

        x = self.conv3(x)
        x = self.bn3(x)
        x = tf.nn.tanh(x)
        x = self.maxpool3(x)

        x = self.conv4(x)
        x = self.bn4(x)
        x = tf.nn.tanh(x)
        x = self.maxpool4(x)

        # FC layers
        x = self.reshape(x)
        x = self.fc1(x)
        x = tf.nn.tanh(x)
        x = self.fc2(x)
        x = tf.nn.tanh(x)

        return x


class SimilarityNet(Model):
    def __init__(self,  nb12=2048, nb3=64):
        super(SimilarityNet, self).__init__()
        w_inititalizer = tf.keras.initializers.VarianceScaling(scale=1.0, mode="fan_avg", distribution="uniform")

        # FC layers
        self.fc1 = Dense(nb12, kernel_initializer=w_inititalizer, bias_initializer="zeros", name="fc1")

        self.multiply = Multiply()
        self.concat = Concatenate(axis=1)

        self.fc2 = Dense(nb3, kernel_initializer=w_inititalizer, bias_initializer="zeros", name="fc2")
        self.fc3 = Dense(2, kernel_initializer=w_inititalizer, bias_initializer="zeros", name="fc2")

    def call(self, f1, f2):
        # FC layers
        fcb1 = tf.nn.relu(self.fc1(f1))
        fcb2 = tf.nn.relu(self.fc1(f2))

        fcb1b2_mult = self.multiply([fcb1, fcb2])
        fcb1b2_concat = self.concat([fcb1, fcb1b2_mult, fcb2])

        fcb3 = tf.nn.relu(self.fc2(fcb1b2_concat))

        output = self.fc3(fcb3)

        return output
