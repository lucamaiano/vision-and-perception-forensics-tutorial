#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: owen
"""

import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects

import tensorflow as tf
from .blockimage import tile_image, span_image_by_overlap
import seaborn as sns

import pickle
from src.utils import load_MISL_weights, load_SimilarityNet_weights
from src.model import MISLNet, SimilarityNet

from tensorflow.python.framework import ops

class SimGraph:
    """Similarity Graph Class

    forensic similarity graph for image forgery detection and localization

    Attributes
    ----------
    mat : NxN numpy array, float
        matrix containing pairwise forensic similarity between image patches
    inds : list of (x,y) int tuples
        list of x,y coordinates that correspond to the top-left corner of each 
        inspected patch in the image. len(inds) = N
    patch_size : int
        size of the patch used

    Parameters
    ----------
    mat : NxN numpy array, float
        matrix containing pairwise forensic similarity between image patches
    inds : list of (x,y) int tuples
        list of x,y coordinates that correspond to the top-left corner of each 
        inspected patch in the image. len(inds) = N
    patch_size : int
        size of the patch used
    """
    
    def __init__(self,mat,inds,patch_size):
        self.mat = mat
        self.inds = inds
        self.patch_size = patch_size
        
    #def to_igraph(weighted=True,threshold=0.0): #convert to igraph representation
    #def to_file() #save to pickle
    
    def plot_matrix(self,palette='GnBu'):
        fig,ax = plt.subplots(1)

        sns.heatmap(self.mat,ax=ax,cmap=sns.color_palette(palette=palette,n_colors=100),
            		    cbar_kws={'label':'Similarity'},linewidths=0.0,vmin=0,vmax=1)
            
        plt.yticks(rotation=0);
        plt.ylabel('Patch 1 Index')
        plt.xlabel('Patch 2 Index')
        plt.tight_layout()
        return fig, ax
    
    def plot_indices(self,ax=None,image=None,fontsize=10):
        if (ax is None) and (image is None): #if don't get an axis to plot on, create one
            fig,ax = plt.subplots(1)
        
        #if we get an image to plot on, create figure with image in it
        if image is not None:
            fig,ax = plt.subplots(1) #create figure and axis
            ax.imshow(image) #plot image
            plt.xticks([]) #remove ticks
            plt.yticks([])
            plt.tight_layout() #remove whitespace

        for i,ind in enumerate(self.inds): #iterate through each index
            #write index on center of its location
    	    tt = ax.text(ind[0]+self.patch_size/2, ind[1]+self.patch_size/2,str(i),
                      color='white', fontsize=fontsize)
            #outline in black so its easy to read
    	    tt.set_path_effects([path_effects.Stroke(linewidth=2, foreground='black'),
                              path_effects.Normal()])
        return ax
    
    def save(self,name):
        with open(name,'wb') as f:
            pickle.dump(self,f,pickle.HIGHEST_PROTOCOL)
                
                               

def from_file(filename):
    with open(filename,'rb') as f:
        sg = pickle.load(f)
    return sg

def softmax(a): #function to calculate softmax
    e = np.exp(a)
    div = np.tile(e.sum(1,keepdims=1),(1,a.shape[1]))
    sm = e/div
    return sm
    
def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]
    

#TWO-STEP Process
def calc_features_insession(X, misl_net, batch_size=48):
    feats_list = []
    
    for ix in batch(range(len(X)),batch_size):
        feature_vect = misl_net(X[ix])
        feats_list.append(feature_vect)
    feats = np.concatenate(feats_list)

    return feats


def compare_features_insession(F1, F2, similarity_net, batch_size=10, verbose=False):
    similarities = []
    xx2 = F2.reshape(F2.shape[0], F2.shape[-1])
    
    for ind in tqdm(range(len(F1)), disable=verbose):
        xx1 = np.tile(F1[ind], (xx2.shape[0], 1))
        for ix in batch(range(len(xx1)), batch_size):
            similarity_score = similarity_net(xx1[ix], xx2[ix])
            similarities.append(softmax(similarity_score))
    
    out_similarity = np.concatenate(similarities)
    
    return out_similarity

def calc_simgraph_insession_twostep(misl_net, similarity_net, image, patch_size,
                                    overlap, span=True, image_batch=48, feature_batch=2056, verbose=False):
    
    if isinstance(image,str): #if input is a string, assume it's a file path
        image = plt.imread(image) #load image
    
    # overlap logic, overlap can either be a tuple=(dx,dy), or an int dx=dy=overlap
    if isinstance(overlap,tuple):
        dx = overlap[0]
        dy = overlap[1]
    else:
        dx = overlap
        dy = overlap
    
    #tile image
    if span:
        X, inds = span_image_by_overlap(image, patch_size, patch_size, dx, dy, snap_to=16)
    else:
        X,inds = tile_image(image, patch_size, patch_size, dx, dy)
    X = X.astype('float32')
    
    feats = calc_features_insession(X, misl_net, batch_size=image_batch)
    sm = compare_features_insession(feats, feats, similarity_net, batch_size=feature_batch, verbose=verbose)
    
    sim = np.array(sm)[:,1] #similarity neuron
    mat = sim.reshape((len(X),len(X))) #convert to matrix form
    sg = SimGraph(mat,inds,patch_size)
    
    return sg

def calc_simgraph(image, patch_size, overlap, weights_dict, run_type='twostep',
                  span=True, batch_size=48, feature_batch=2056, verbose=False):
    
    if isinstance(image,str): #if input is a string, assume it's a file path
        image = plt.imread(image) #load image
    
    if run_type == 'twostep':
        
        misl_net = MISLNet(input_size=patch_size)
        similarity_net = SimilarityNet()

        misl_net(tf.random.uniform((batch_size, patch_size, patch_size, 3)))
        similarity_net(tf.random.uniform((feature_batch, 200)), tf.random.uniform((feature_batch, 200)))
        load_MISL_weights(misl_net, weights_dict)
        load_SimilarityNet_weights(similarity_net, weights_dict)
        
        return calc_simgraph_insession_twostep(misl_net, similarity_net,
                                                    image, patch_size, overlap, span=span, image_batch = batch_size,feature_batch = feature_batch, verbose=verbose)



def compare_features_similarity(F1, F2, similarity_net, batch_size=10, verbose=False):
    similarities = []
    xx2 = F2.reshape(F2.shape[0], F2.shape[-1])
    
    for ind in tqdm(range(len(F1)), disable=verbose):
        xx1 = np.tile(F1[ind], (xx2.shape[0], 1))
        for ix in batch(range(len(xx1)), batch_size):
            similarity_score = similarity_net(xx1[ix], xx2[ix])
            similarities.append(softmax(similarity_score)[:,1]) #second value is similarity (first value/0th index is dissimilarity and not used)
    
    out_similarity = np.hstack(similarities)
    
    return out_similarity


def calc_simg_insession_twostep(misl_net, similarity_net, image_1, image_2, patch_size,
                                    overlap, span=True, image_batch=48, feature_batch=2056, N = 1000, verbose=False):
    
    if isinstance(image_1,str): #if input is a string, assume it's a file path
        image_1 = plt.imread(image_1) #load image
    if isinstance(image_2,str): #if input is a string, assume it's a file path
        image_2 = plt.imread(image_2) #load image
    
    # overlap logic, overlap can either be a tuple=(dx,dy), or an int dx=dy=overlap
    if isinstance(overlap,tuple):
        dx = overlap[0]
        dy = overlap[1]
    else:
        dx = overlap
        dy = overlap
    
    #tile image
    if span:
        X1, _ = span_image_by_overlap(image_1, patch_size, patch_size, dx, dy, snap_to=16)
        X2, _ = span_image_by_overlap(image_2, patch_size, patch_size, dx, dy, snap_to=16)
    else:
        X1, _ = tile_image(image_1, patch_size, patch_size, dx, dy)
        X2, _ = tile_image(image_2, patch_size, patch_size, dx, dy)

    #Randomly select N tiles
    inds1 = np.random.randint(0,len(X1),size=N) 
    inds2 = np.random.randint(0,len(X2),size=N)

    X1 = X1[inds1] #vector of randomly selected image tiles
    X2 = X2[inds2] #vector of randomly selected image tiles
    X1 = X1.astype('float32')
    X2 = X2.astype('float32')
    
    feats_1 = calc_features_insession(X1, misl_net, batch_size=image_batch)
    feats_2 = calc_features_insession(X2, misl_net, batch_size=image_batch)
    sm = compare_features_similarity(feats_1, feats_2, similarity_net, batch_size=feature_batch, verbose=verbose)
    
    return sm


def calc_similarity(image_1, image_2, patch_size, overlap, weights_dict, run_type='twostep',
                  span=True, batch_size=48, feature_batch=2056, verbose=False):
    
    if isinstance(image_1,str): #if input is a string, assume it's a file path
        image_1 = plt.imread(image_1) #load image_1
    if isinstance(image_2,str): #if input is a string, assume it's a file path
        image_2 = plt.imread(image_2) #load image_2
    
    if run_type == 'twostep':
        
        misl_net = MISLNet(input_size=patch_size)
        similarity_net = SimilarityNet()

        misl_net(tf.random.uniform((batch_size, patch_size, patch_size, 3)))
        similarity_net(tf.random.uniform((feature_batch, 200)), tf.random.uniform((feature_batch, 200)))
        load_MISL_weights(misl_net, weights_dict)
        load_SimilarityNet_weights(similarity_net, weights_dict)
        
        return calc_simg_insession_twostep(misl_net, similarity_net, image_1, image_2, patch_size, overlap, 
                                                    span=span, image_batch = batch_size,feature_batch = feature_batch, verbose=verbose)

