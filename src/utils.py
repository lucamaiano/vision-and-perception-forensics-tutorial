import tensorflow as tf
from tensorflow.python.training import py_checkpoint_reader


def load_pretrained_weights(patch_size):
    if patch_size in [128, 256]:
        fweights = f'./models/cam_{patch_size}/-30' #path to model CNN weights
    else:
        raise TypeError('Unsupported patch size {}'.format(patch_size))
    reader = py_checkpoint_reader.NewCheckpointReader(fweights)

    weights_dict = {
        v: reader.get_tensor(v) for v in reader.get_variable_to_shape_map()
    }
    return weights_dict

def load_MISL_weights(model, state_dict):
    model.constrained.set_weights([state_dict['MISLNet/weights_cstr']])

    model.conv1.set_weights([state_dict['MISLNet/weights_conv1'], state_dict['MISLNet/bias_conv1']])
    model.bn1.beta = state_dict['MISLNet/batch_normalization/beta']
    model.bn1.gamma = state_dict['MISLNet/batch_normalization/gamma']
    model.bn1.moving_mean = state_dict['MISLNet/batch_normalization/moving_mean']
    model.bn1.moving_variance = state_dict['MISLNet/batch_normalization/moving_variance']

    model.conv2.set_weights([state_dict['MISLNet/weights_conv2'], state_dict['MISLNet/bias_conv2']])
    model.bn2.beta = state_dict['MISLNet/batch_normalization_1/beta']
    model.bn2.gamma = state_dict['MISLNet/batch_normalization_1/gamma']
    model.bn2.moving_mean = state_dict['MISLNet/batch_normalization_1/moving_mean']
    model.bn2.moving_variance = state_dict['MISLNet/batch_normalization_1/moving_variance']


    model.conv3.set_weights([state_dict['MISLNet/weights_conv3'], state_dict['MISLNet/bias_conv3']])
    model.bn3.beta = state_dict['MISLNet/batch_normalization_2/beta']
    model.bn3.gamma = state_dict['MISLNet/batch_normalization_2/gamma']
    model.bn3.moving_mean = state_dict['MISLNet/batch_normalization_2/moving_mean']
    model.bn3.moving_variance = state_dict['MISLNet/batch_normalization_2/moving_variance']

    model.conv4.set_weights([state_dict['MISLNet/weights_conv4'], state_dict['MISLNet/bias_conv4']])
    model.bn4.beta = state_dict['MISLNet/batch_normalization_3/beta']
    model.bn4.gamma = state_dict['MISLNet/batch_normalization_3/gamma']
    model.bn4.moving_mean = state_dict['MISLNet/batch_normalization_3/moving_mean']
    model.bn4.moving_variance = state_dict['MISLNet/batch_normalization_3/moving_variance']

    model.fc1.set_weights([state_dict['MISLNet/weights_d1'], state_dict['MISLNet/bias_d1']])
    model.fc2.set_weights([state_dict['MISLNet/weights_d2'], state_dict['MISLNet/bias_d2']])

def load_SimilarityNet_weights(model, state_dict):
    model.fc1.set_weights([state_dict['CompareNet/weights_fcb1'], state_dict['CompareNet/bias_fcb1']])
    model.fc2.set_weights([state_dict['CompareNet/weights_fcb3'], state_dict['CompareNet/bias_fcb3']])
    model.fc3.set_weights([state_dict['CompareNet/weights_out'], state_dict['CompareNet/bias_out']])
  